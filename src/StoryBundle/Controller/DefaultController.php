<?php

namespace StoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use StoryBundle\Entity\Story;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    public function addStoryAction(Request $request)
    {
        $story = new Story();
        $repository = $this->getDoctrine()->getManager()->getRepository('StoryBundle:Story');
        
        $form = $this->createFormBuilder($story)
            ->add('title', TextType::class)
            ->add('content', TextareaType::class)
            ->add('submit', SubmitType::class, array('label' => 'Post'))
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            
            $date = new \DateTime(date("Y-m-d H:i:s"));
            
            $story->setScore('0');
            $story->setDate($date);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($story);
            $em->flush();
            
//            $request->getSession()->getFlashBag()->add('notice', 'Story ajoutée !');
            return $this->redirect($this->generateUrl('app_home'), 301);
        }
        
        return $this->render('StoryBundle:Default:add-story.html.twig', array('form' => $form->createView()));
    }
    
    public function upvoteStoryAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('StoryBundle:Story');
        $story = $repository->find($id);
        $session = new Session();
        
        if ($session->get('votedStory' + $story->getId())) {
            return $this->redirect($this->generateUrl('app_home'), 301);
        }
        
        $story->setScore($story->getScore() + 1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($story);
        $em->flush();
        $session->set('votedStory' + $story->getId(), true);
        
        return $this->redirect($this->generateUrl('app_home'), 301);
    }

    public function downvoteStoryAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('StoryBundle:Story');
        $story = $repository->find($id);
        $session = new Session();
        
        if ($session->get('votedStory' + $story->getId())) {
            return $this->redirect($this->generateUrl('app_home'), 301);
        }
        
        $story->setScore($story->getScore() - 1);
        $em = $this->getDoctrine()->getManager();
        $em->persist($story);
        $em->flush();
        $session->set('votedStory' + $story->getId(), true);
        
        return $this->redirect($this->generateUrl('app_home'), 301);
    }
}
