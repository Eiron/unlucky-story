<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use StoryBundle\Entity\Story;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('StoryBundle:Story');
        $stories = $repository->orderedFindAll();
        
        return $this->render('AppBundle:Default:index.html.twig', array('stories' => $stories));
    }

    public function topAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('StoryBundle:Story');
        $stories = $repository->getTop30();
        
        return $this->render('AppBundle:Default:top.html.twig', array('stories' => $stories));
    }

    public function flopAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('StoryBundle:Story');
        $stories = $repository->getFlop30();
        
        return $this->render('AppBundle:Default:flop.html.twig', array('stories' => $stories));
    }
}
